## GWIStat 

GWIStat tracks the real-time status of gravitational wave interferometers in the LIGO-Virgo-KAGRA Collaboration. 
For current detector status and estimated range history, see [online.ligo.org](https://online.ligo.org/) 



DAG we're trying to replace:

```bash
# gw_ifo_stat.dag
# BEGIN META
# END META
# BEGIN NODES AND EDGES
JOB gwistat_geo600_collector.00000 gwistat_geo600_collector.sub
VARS gwistat_geo600_collector.00000 nodename="gwistat_geo600_collector_00000" 
                                    verbose="--verbose" 
                                    scald_config="--scald-config web/scald_config.yml"
RETRY gwistat_geo600_collector.00000 1000

JOB gstlal_ifo_stat.00000 gstlal_ifo_stat.sub
VARS gstlal_ifo_stat.00000 nodename="gstlal_ifo_stat_00000" 
                           channel_name="--channel-name H1=GDS-CALIB_STATE_VECTOR" 
                           shared_memory_dir="--shared-memory-dir H1=/dev/shm/kafka/H1" 
                           data_source="--data-source devshm" 
                           bootstrap_from_influx="--bootstrap-from-influx" 
                           verbose="--verbose" 
                           scald_config="--scald-config web/scald_config.yml"
RETRY gstlal_ifo_stat.00000 1000

JOB gstlal_ifo_stat.00001 gstlal_ifo_stat.sub
VARS gstlal_ifo_stat.00001 nodename="gstlal_ifo_stat_00001" 
                           channel_name="--channel-name L1=GDS-CALIB_STATE_VECTOR" 
                           shared_memory_dir="--shared-memory-dir L1=/dev/shm/kafka/L1" 
                           data_source="--data-source devshm" 
                           bootstrap_from_influx="--bootstrap-from-influx" 
                           verbose="--verbose" 
                           scald_config="--scald-config web/scald_config.yml"
RETRY gstlal_ifo_stat.00001 1000

JOB gstlal_ifo_stat.00002 gstlal_ifo_stat.sub
VARS gstlal_ifo_stat.00002 nodename="gstlal_ifo_stat_00002" 
                           channel_name="--channel-name V1=DQ_ANALYSIS_STATE_VECTOR" 
                           shared_memory_dir="--shared-memory-dir V1=/dev/shm/kafka/V1" 
                           data_source="--data-source devshm" 
                           bootstrap_from_influx="--bootstrap-from-influx" 
                           verbose="--verbose" 
                           scald_config="--scald-config web/scald_config.yml"
RETRY gstlal_ifo_stat.00002 1000

JOB gstlal_ifo_stat.00003 gstlal_ifo_stat.sub
VARS gstlal_ifo_stat.00003 nodename="gstlal_ifo_stat_00003" 
                           channel_name="--channel-name K1=CAL-STATE_VECTOR_C00" 
                           shared_memory_dir="--shared-memory-dir K1=/dev/shm/kafka/K1" 
                           data_source="--data-source devshm" 
                           bootstrap_from_influx="--bootstrap-from-influx" 
                           verbose="--verbose" 
                           scald_config="--scald-config web/scald_config.yml"
RETRY gstlal_ifo_stat.00003 1000

JOB gstlal_ll_dq.00000 gstlal_ll_dq.sub
VARS gstlal_ll_dq.00000 nodename="gstlal_ll_dq_00000"  
                        data_source="--data-source devshm" 
                        shared_memory_dir="--shared-memory-dir H1=/dev/shm/kafka/H1" 
                        shared_memory_block_size="--shared-memory-block-size 4096" 
                        shared_memory_assumed_duration="--shared-memory-assumed-duration 1" 
                        psd_fft_length="--psd-fft-length 16" 
                        channel_name="--channel-name H1=GDS-CALIB_STRAIN_CLEAN" 
                        state_channel_name="--state-channel-name H1=GDS-CALIB_STATE_VECTOR" 
                        dq_channel_name="--dq-channel-name H1=DMT-DQ_VECTOR" 
                        state_vector_on_bits="--state-vector-on-bits H1=3" 
                        state_vector_off_bits="--state-vector-off-bits H1=0" 
                        dq_vector_on_bits="--dq-vector-on-bits H1=0" 
                        dq_vector_off_bits="--dq-vector-off-bits H1=0" 
                        scald_config="--scald-config web/scald_config.yml" 
                        output_kafka_server="--output-kafka-server kafkagstlal.ldas.cit:9196" 
                        analysis_tag="--analysis-tag gwistat" 
                        reference_psd="--reference-psd psd/H1K1L1V1-REFERENCE_PSD-0-0.xml.gz" 
                        horizon_approximant="--horizon-approximant TaylorF2" 
                        horizon_f_min="--horizon-f-min 10" 
                        horizon_f_max="--horizon-f-max 1570"
RETRY gstlal_ll_dq.00000 1000

JOB gstlal_ll_dq.00001 gstlal_ll_dq.sub
VARS gstlal_ll_dq.00001 nodename="gstlal_ll_dq_00001"  
                        data_source="--data-source devshm" 
                        shared_memory_dir="--shared-memory-dir L1=/dev/shm/kafka/L1" 
                        shared_memory_block_size="--shared-memory-block-size 4096" 
                        shared_memory_assumed_duration="--shared-memory-assumed-duration 1" 
                        psd_fft_length="--psd-fft-length 16" 
                        channel_name="--channel-name L1=GDS-CALIB_STRAIN_CLEAN" 
                        state_channel_name="--state-channel-name L1=GDS-CALIB_STATE_VECTOR" 
                        dq_channel_name="--dq-channel-name L1=DMT-DQ_VECTOR" 
                        state_vector_on_bits="--state-vector-on-bits L1=3" 
                        state_vector_off_bits="--state-vector-off-bits L1=0" 
                        dq_vector_on_bits="--dq-vector-on-bits L1=0" 
                        dq_vector_off_bits="--dq-vector-off-bits L1=0" 
                        scald_config="--scald-config web/scald_config.yml" 
                        output_kafka_server="--output-kafka-server kafkagstlal.ldas.cit:9196" 
                        analysis_tag="--analysis-tag gwistat" 
                        reference_psd="--reference-psd psd/H1K1L1V1-REFERENCE_PSD-0-0.xml.gz" 
                        horizon_approximant="--horizon-approximant TaylorF2" 
                        horizon_f_min="--horizon-f-min 10" 
                        horizon_f_max="--horizon-f-max 1570"
RETRY gstlal_ll_dq.00001 1000

JOB gstlal_ll_dq.00002 gstlal_ll_dq.sub
VARS gstlal_ll_dq.00002 nodename="gstlal_ll_dq_00002"  
                        data_source="--data-source devshm" 
                        shared_memory_dir="--shared-memory-dir V1=/dev/shm/kafka/V1" 
                        shared_memory_block_size="--shared-memory-block-size 4096" 
                        shared_memory_assumed_duration="--shared-memory-assumed-duration 1" 
                        psd_fft_length="--psd-fft-length 16" 
                        channel_name="--channel-name V1=Hrec_hoft_16384Hz" 
                        state_channel_name="--state-channel-name V1=DQ_ANALYSIS_STATE_VECTOR" 
                        dq_channel_name="--dq-channel-name V1=DQ_ANALYSIS_STATE_VECTOR" 
                        state_vector_on_bits="--state-vector-on-bits V1=3" 
                        state_vector_off_bits="--state-vector-off-bits V1=0" 
                        dq_vector_on_bits="--dq-vector-on-bits V1=0" 
                        dq_vector_off_bits="--dq-vector-off-bits V1=0" 
                        scald_config="--scald-config web/scald_config.yml" 
                        output_kafka_server="--output-kafka-server kafkagstlal.ldas.cit:9196" 
                        analysis_tag="--analysis-tag gwistat" 
                        reference_psd="--reference-psd psd/H1K1L1V1-REFERENCE_PSD-0-0.xml.gz" horizon_approximant="--horizon-approximant TaylorF2" horizon_f_min="--horizon-f-min 10" horizon_f_max="--horizon-f-max 1570"
RETRY gstlal_ll_dq.00002 1000

JOB gstlal_ll_dq.00003 gstlal_ll_dq.sub
VARS gstlal_ll_dq.00003 nodename="gstlal_ll_dq_00003"  
                        data_source="--data-source devshm" 
                        shared_memory_dir="--shared-memory-dir K1=/dev/shm/kafka/K1" 
                        shared_memory_block_size="--shared-memory-block-size 4096" 
                        shared_memory_assumed_duration="--shared-memory-assumed-duration 1" 
                        psd_fft_length="--psd-fft-length 16" 
                        channel_name="--channel-name K1=CAL-STRAIN_C00" 
                        state_channel_name="--state-channel-name K1=CAL-STATE_VECTOR_C00" 
                        dq_channel_name="--dq-channel-name K1=CAL-STATE_VECTOR_C00" 
                        state_vector_on_bits="--state-vector-on-bits K1=1" 
                        state_vector_off_bits="--state-vector-off-bits K1=0" 
                        dq_vector_on_bits="--dq-vector-on-bits K1=0" 
                        dq_vector_off_bits="--dq-vector-off-bits K1=0" 
                        scald_config="--scald-config web/scald_config.yml" 
                        output_kafka_server="--output-kafka-server kafkagstlal.ldas.cit:9196" 
                        analysis_tag="--analysis-tag gwistat" 
                        reference_psd="--reference-psd psd/H1K1L1V1-REFERENCE_PSD-0-0.xml.gz" 
                        horizon_approximant="--horizon-approximant TaylorF2" 
                        horizon_f_min="--horizon-f-min 10" 
                        horizon_f_max="--horizon-f-max 1570"
RETRY gstlal_ll_dq.00003 1000
# END NODES AND EDGES
```

with the following subs:
```bash 
# gstlal_ll_dq.sub
universe = vanilla
executable = /usr/bin/gstlal_ll_dq
arguments = $(data_source) $(shared_memory_dir) $(shared_memory_block_size) $(shared_memory_assumed_duration) $(psd_fft_length) $(channel_name) $(state_channel_name) $(dq_channel_name) $(state_vector_on_bits) $(state_vector_off_bits) $(dq_vector_on_bits) $(dq_vector_off_bits) $(scald_config) $(output_kafka_server) $(analysis_tag) $(reference_psd) $(horizon_approximant) $(horizon_f_min) $(horizon_f_max)
periodic_release = (HoldReasonCode == 5) || ((CurrentTime - EnteredCurrentStatus > 180) && (HoldReasonCode != 34))
request_cpus = 1
request_memory = ( MemoryUsage ) * 3 / 2
request_disk = 1GB
accounting_group_user = chad.hanna
accounting_group = ligo.dev.o4.cbc.em.gstlalonline
MY.SingularityImage = "/home/gwistat/observing/4/a/builds/gstlal_c0b2feb3/"
getenv = INFLUX_*,GWISTAT_GEO600*
transfer_executable = False
should_transfer_files = YES
requirements = (HAS_SINGULARITY=?=True)
when_to_transfer_output = ON_SUCCESS
preserve_relative_paths = True
transfer_input_files = web/scald_config.yml,psd/H1K1L1V1-REFERENCE_PSD-0-0.xml.gz
environment = "GSTLAL_FIR_WHITEN=0 PYTHONUNBUFFERED=1 "
output = logs/$(nodename)-$(cluster)-$(process).out
error = logs/$(nodename)-$(cluster)-$(process).err
notification = never
MY.MemoryUsage = ( 2000 ) * 2 / 3

queue
```

```bash
# gstlal_ifo_stat.sub
universe = vanilla
executable = /usr/bin/gstlal_ifo_stat
arguments = $(channel_name) $(shared_memory_dir) $(data_source) $(bootstrap_from_influx) $(verbose) $(scald_config)
periodic_release = (HoldReasonCode == 5) || ((CurrentTime - EnteredCurrentStatus > 180) && (HoldReasonCode != 34))
request_cpus = 1
request_memory = ( MemoryUsage ) * 3 / 2
request_disk = 1GB
accounting_group_user = chad.hanna
accounting_group = ligo.dev.o4.cbc.em.gstlalonline
MY.SingularityImage = "/home/gwistat/observing/4/a/builds/gstlal_c0b2feb3/"
getenv = INFLUX_*,GWISTAT_GEO600*
transfer_executable = False
should_transfer_files = YES
requirements = (HAS_SINGULARITY=?=True)
when_to_transfer_output = ON_SUCCESS
preserve_relative_paths = True
transfer_input_files = web/scald_config.yml
environment = "GSTLAL_FIR_WHITEN=0 PYTHONUNBUFFERED=1 "
output = logs/$(nodename)-$(cluster)-$(process).out
error = logs/$(nodename)-$(cluster)-$(process).err
notification = never
MY.MemoryUsage = ( 2000 ) * 2 / 3

```

```bash
# gwistat_geo600_collector.sub
universe = vanilla
executable = `/home/gwistat/observing/4/a/gwistat/python/gwistat_geo600_collector`
arguments = $(verbose) $(scald_config)
periodic_release = (HoldReasonCode == 5) || ((CurrentTime - EnteredCurrentStatus > 180) && (HoldReasonCode != 34))
request_cpus = 1
request_memory = ( MemoryUsage ) * 3 / 2
request_disk = 1GB
accounting_group_user = chad.hanna
accounting_group = ligo.dev.o4.cbc.em.gstlalonline
MY.SingularityImage = "/home/gwistat/observing/4/a/builds/gstlal_c0b2feb3/"
getenv = INFLUX_*,GWISTAT_GEO600*
transfer_executable = False
should_transfer_files = YES
requirements = (HAS_SINGULARITY=?=True)
when_to_transfer_output = ON_SUCCESS
preserve_relative_paths = True
transfer_input_files = web/scald_config.yml
environment = "GSTLAL_FIR_WHITEN=0 PYTHONUNBUFFERED=1 "
output = logs/$(nodename)-$(cluster)-$(process).out
error = logs/$(nodename)-$(cluster)-$(process).err
notification = never
MY.MemoryUsage = ( 2000 ) * 2 / 3

queue
```
