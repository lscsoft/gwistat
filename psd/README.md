## Reference PSD

* Hanford and Livingston PSD is computed from ~1 week of O4a data
* KAGRA PSD is computed from the DARM sensitivity at https://gwdoc.icrr.u-tokyo.ac.jp/cgi-bin/private/DocDB/ShowDocument?docid=15005
* Virgo PSD is made from the ASD column in https://dcc.ligo.org/LIGO-G2400496
