#!/usr/bin/env python3

import argparse
import os
import sys
import yaml

from gstlal.config.inspiral import Config
from gstlal.dags.inspiral import DAG
from gstlal.dags import Argument, Option
from gstlal.workflows import write_makefile
from gstlal.dags.layers import Layer, Node
from gstlal.dags import util as dagutil

parser = argparse.ArgumentParser()
subparser = parser.add_subparsers(title="commands", metavar="<command>", dest="command")
subparser.required = True
p = subparser.add_parser("create", help="")
p.add_argument("-c", "--config", help="Sets the path to read configuration from.")
p.add_argument("-n", "--dag-name", default = "gw_ifo_stat", help = "Provide a name for the output .dag file. Default is gw_ifo_stat.")

args = parser.parse_args()

with open(args.config, "r") as f:
	config = yaml.safe_load(f)

dag = DAG(config={})
dag.create_log_dir()

ifos = config["instruments"]
ifos = [ifos[2*n:2*n+2] for n in range(len(ifos) // 2)]

requirements = {
	"request_cpus": 1,
	"request_memory": 2000,
	"request_disk": "1GB",
	"accounting_group_user": config["condor"]["accounting_group_user"],
	"accounting_group": config["condor"]["accounting_group"],
	"MY.SingularityImage": f"\"{config['condor']['singularity']}\"",
	"getenv": "INFLUX_*,GWISTAT_GEO600*",
}

env_variables = config["condor"]["environment"]
env_string = "\""
for var in env_variables:
	for k, v in var.items():
		env_string += f"{k}={v} "
env_string += "\""
requirements.update({"environment": env_string})

##### add GEO600 layer ####
layer = Layer(os.path.join(config["run-dir"], "python/gwistat_geo600_collector"),
		requirements=requirements,
		transfer_files=config["condor"]["transfer_files"],
		dynamic_memory=True,
		retries=1000,
)

layer += Node(
	arguments = [Option("verbose")],
	inputs = [Option("scald-config", config["scald"])]
)

dag.attach(layer)

##### add gstlal_ifo_stat layer ####
layer = Layer("gstlal_ifo_stat",
		requirements=requirements,
		transfer_files=config["condor"]["transfer_files"],
		dynamic_memory=True,
		retries=1000,
)

for ifo in ifos:
	common_opts = [
		Option("channel-name", dagutil.format_ifo_args(ifo, config["source"]["state-channel-name"])),
		Option("shared-memory-dir", dagutil.format_ifo_args(ifo, config["source"]["shared-memory-dir"])),
		Option("data-source", "devshm"),
		Option("bootstrap-from-influx"),
		Option("verbose")
	]

	inputs = [
		Option("scald-config", config['scald'])
	]

	layer += Node(
		arguments = common_opts,
		inputs = inputs,
	)

dag.attach(layer)

##### add gstlal_ll_dq layer ####
layer = Layer(
	"gstlal_ll_dq",
	requirements=requirements,
	transfer_files=config["condor"]["transfer_files"],
	dynamic_memory=True,
	retries=1000,
)

source = config["source"]
for ifo in ifos:
	channel_names = [source["channel-name"][ifo]]
	for channel in channel_names:
		# set up datasource options
		arguments = [
			Option("data-source", "devshm"),
			Option("shared-memory-dir", dagutil.format_ifo_args(ifo, source["shared-memory-dir"])),
			Option("shared-memory-block-size", source["shared-memory-block-size"]),
			Option("shared-memory-assumed-duration", source["shared-memory-assumed-duration"]),
		]

		arguments.extend([
			Option("psd-fft-length", 16),
			Option("channel-name", f"{ifo}={channel}"),
			Option("state-channel-name", f"{ifo}={source['state-channel-name'][ifo]}"),
			Option("dq-channel-name", f"{ifo}={source['dq-channel-name'][ifo]}"),
			Option("state-vector-on-bits", f"{ifo}={source['state-vector-on-bits'][ifo]}"),
			Option("state-vector-off-bits", f"{ifo}={source['state-vector-off-bits'][ifo]}"),
			Option("dq-vector-on-bits", f"{ifo}={source['dq-vector-on-bits'][ifo]}"),
			Option("dq-vector-off-bits", f"{ifo}={source['dq-vector-off-bits'][ifo]}"),
			Option("scald-config", config['scald']),
			Option("output-kafka-server", config['services']['kafka-server']),
			Option("analysis-tag", config['tag']),
			Option("reference-psd", config['reference-psd']),
			Option("horizon-approximant", config['approximant']),
			Option("horizon-f-min", config['f-min']),
			Option("horizon-f-max", config['f-max'])
		])
		layer += Node(arguments = arguments)

dag.attach(layer)

dag_name = args.dag_name
dag.write_dag(f"{dag_name}.dag")
dag.write_script(f"{dag_name}.sh")
