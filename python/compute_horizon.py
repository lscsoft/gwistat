#!/usr/bin/env python3

from gstlal import psd

horizon_distance_func = psd.HorizonDistance(10., 1570., 1./16., 1.4, 1.4, approximant = 'TaylorF2')

reference_psd = psd.read_psd('psd/H1K1L1V1-REFERENCE_PSD-0-0.xml.gz', verbose = True)

for ifo, psd in reference_psd.items():
	ref_range = horizon_distance_func(psd, 8)[0] / 2.25

	print(f'{ifo}: {ref_range} (Mpc)')
