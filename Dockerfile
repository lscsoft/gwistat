# FIXME: might want to consider tagging a
# branch instead of using master
FROM containers.ligo.org/lscsoft/gstlal:master as base

LABEL name="gwistat production container" \
	  date="2023-10-20"

RUN dnf update -y &&\
    dnf clean all && \
    rm -rf /var/cache/dnf

# ADD config and data files
COPY config.yml gwistat/config.yml
# COPY influx_creds.sh.sample gwistat/influx_creds.sh
# COPY web/scald_config-dev.yml /web/scald_config.yml
COPY psd/H1K1L1V1-REFERENCE_PSD-0-0.xml.gz /psd/H1K1L1V1-REFERENCE_PSD-0-0.xml.gz

# ADD any executables
COPY python/gwistat_geo600_collector gwistat/gwistat_geo600_collector
COPY python/gstlal_ifo_stat /usr/bin/gstlal_ifo_stat
#COPY python/write_dag.py gwistat/write_dag.py
